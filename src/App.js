import React from 'react';
import Navigation from "./components/Navigation.js";
import {useDispatch} from 'react-redux';
import {setLoggedUser} from './redux/actions/index.js';
function App() {
  const dispatch = useDispatch();
  const userInfos = JSON.parse(localStorage.getItem("myNewsUserInfos"));
  if(userInfos){
    const loggedInfos = {
        isLogged : true,
        userName : userInfos.name,
        loggedUserId : userInfos.jwt
    }
    dispatch(setLoggedUser(loggedInfos));
  }
  return (
    <div className="App">
      <Navigation/>
    </div>
  );
}

export default App;
