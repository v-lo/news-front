import React from "react";
import Icon from '@material-ui/core/Icon';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';

export default function sourceChips(props) {
    const sourceChips = props.sourceChips;
    return (
        sourceChips.map((chip, i) => (
            <Chip
                avatar={<Avatar><Icon>{chip.icon}</Icon></Avatar>}
                label={chip.text}
                className="source-chip-item"
                key={i}
            />
        ))
    );
}