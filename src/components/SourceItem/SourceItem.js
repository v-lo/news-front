import React, { Component } from 'react';
import SourceChips from './sourceChips.js';
import Paper from '@material-ui/core/Paper';
import Grow from '@material-ui/core/Grow';
import LinkIcon from '@material-ui/icons/Link';
//import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';

class SourceItem extends Component {
    _isMounted = false;
    timer= false;
    state={
        growed:false,
        i: this.props.i,
        source : this.props.sourceDetails,
        sourceChipsList :[
            {
                text: this.props.sourceDetails.category ,
                icon: "style"
            },
            {
                text: this.props.sourceDetails.country ,
                icon: "language"
            },
            {
                text: this.props.sourceDetails.language ,
                icon: "translate"
            }
        ]
        
    }
    componentDidMount(){
        this._isMounted = true;
        this.timer = setTimeout(() => { this.setState({ growed: !this.state.growed }) }, 40 * this.state.i );
    }
    componentWillUnmount() {
        this._isMounted = false;
        clearTimeout(this.timer);
    }
    render() {
        const {growed, source, sourceChipsList} = this.state;
        return (
            <Grow in={growed} timeout={250}>
                <Paper className="source-item flex-section">
                    <div className="source-content">
                        <div className="source-infos">
                            <h3>{source.name}</h3>
                            <div className="source-chips-container">
                                <SourceChips sourceChips={sourceChipsList}/>
                            </div>
                        </div>
                        <div className="chapo">
                            <p className="source-text">{source.description}</p>
                        </div>
                        <div className="source-item-button-container">
                            <Button href={source.url} target="_blank" size="small" className="source-action-button">
                                Site web
                                <LinkIcon/>
                            </Button>
                            {/* Save button for when user connected can save source */}
                            {/* <Button target="_blank" size="small" className="source-action-button important-cta">
                                Sauvegarder
                                <AddIcon/>
                            </Button> */}
                        </div>
                    </div>
                </Paper>
            </Grow>
        )}
    }

export default SourceItem;