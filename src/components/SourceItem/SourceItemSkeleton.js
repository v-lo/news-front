import React,{useEffect, useState} from "react";
import Paper from "@material-ui/core/Paper";
import Skeleton from "@material-ui/lab/Skeleton";
import Fade from '@material-ui/core/Grow';


export default function SkeletonOfSource(props) {
    const [faded, activateFade] = useState(false);
    const i = props.i;
    useEffect(() => {
        const timer = setTimeout(() => {
            activateFade(true);
        }, (i*20));
        return () => clearTimeout(timer);
      });
  return (
    <Fade in={faded} timeout={250}>
      <Paper className="source-item-skeleton">
          <div className="source-content-skeleton">
                <div className="skeleton-info">
                    <Skeleton disableAnimate={true} variant="rect" height={40} width="100%"/>
                </div>
                <div className="skeleton-text">
                    <Skeleton disableAnimate={true} variant="text" height={15} width="100%"/>
                    <Skeleton disableAnimate={true} variant="text" height={15} width="40%"/>
                </div>
                <div className="skeleton-button">
                    <Skeleton disableAnimate={true} className="button" variant="rect" height={30} width={100}/>
                </div>
          </div>
      </Paper>
    </Fade>
  );
}