import React from "react";
import {useSelector} from 'react-redux';
import Icon from '@material-ui/core/Icon';

export default function AppContextTitle() {
    const title = useSelector(state => state.appContext.label);
    const icon = useSelector(state => state.appContext.icone);
    return (
        <div className="title-container">
            <div className="title-item">
                <Icon>{icon}</Icon>
                <h2>{title}</h2>
            </div>
        </div>
    );
}