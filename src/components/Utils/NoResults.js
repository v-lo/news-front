import React, { Component } from 'react';
import Icon from '@material-ui/core/Icon';

class NoResults extends Component {
    render() {
        return (
            <div className="error-container">
                <Icon>sentiment_dissatisfied</Icon>
                <p>Aucun résultat.</p>
            </div>
        )}
}
export default NoResults;