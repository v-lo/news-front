import React from "react";
import {useSelector} from 'react-redux';
import BookmarkIcon from '@material-ui/icons/BookmarkSharp';

export default function ResultsCount() {
    const articlesResultsNumber = useSelector(state => state.articlesResultsNumber);
    const fetchStatus = useSelector(state => state.fetchStatus);
    const multipleResults = articlesResultsNumber > 1;
    const largeResults = articlesResultsNumber > 999 ? 'large-results' : '';
    return (
        fetchStatus === "ok" && articlesResultsNumber>0 &&
        <div className="results-count-container">
            <BookmarkIcon>bookmark</BookmarkIcon>
            <div className="count-libelle-container">
                <p className={largeResults}>{articlesResultsNumber}</p>
                <span>article{multipleResults && "s"}</span>
            </div>
        </div>
    );
}