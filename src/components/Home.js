import AppContextTitle from './Utils/AppContextTitle.js';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {setAppContext} from './../redux/actions/index.js';
import HomeAuthentication from './Authentication/HomeAuthentication.js';

class Home extends Component {
    componentDidMount(){
        this.props.setAppContext(this.props.appContext);
    }
    render() {
        return (
            <div className="content-container">
                <AppContextTitle/>
                <div className="page-intro">
                    <h2>Bienvenue !</h2>
                    <p>Ce site est basé sur l'API <span>News API</span> : c'est un agrégateur d'articles de presse.</p>
                    <p>Vous pouvez effectuer 3 types de recherche : <span>Articles récents</span>, <span>Tous les articles</span> et <span>Sources</span>. La recerche est paramétrable via des filtres, accessibles via le <span>boutton +</span> situé en haut de page.</p>
                </div>
                <div className="page-intro">
                    <h2>À l'attention des développeurs</h2>
                    <p>Les développements sont basés sur l'environnement technique suivant : <span>ReactJS</span> 16, <span>Redux</span> 4 + <span>react-redux</span> 7 pour la gestion d'état de l'application, <span>Axios</span> pour les appels à l'API, <span>Material UI</span> et <span>Sass</span> pour l'intégration.</p>
                    <p>Pour suivre l'état de l'application : vous pouvez vous servir de l'extension navigateur <span>Redux DevTools</span>. Il vous suffira d'au préalable l'installer sur votre navigateur.</p>
                </div>
                <div className="page-intro">
                    <h2>Évolutions à venir</h2>
                    <p>Un back basé sur <span>NodeJS</span>, <span>ExpressJS</span> et <span>MongoDB</span> est en cours de réalisation. Il permettra à l'utilisateur de s'authentifier (déjà OK) puis de sauvegarder une liste de sources favorites et des collections de recherches.</p>
                    <p>Les appels s'effectueront en utilisant un <span>token JWT</span> fourni par le back et disposé dans les headers de toute requête au back, ce afin de garantir l'intégrité de l'utilisateur qui sollicite l'API.</p>
                </div>
                <div className="page-intro">
                    <h2>Authentification</h2>
                    <p>Pour utiliser le formulaire de création de compte/connexion, vous devez au préalable faire tourner le projet back sur votre machine local (utilise le port 8080 - se lance via la commande npm start).</p>
                    <HomeAuthentication/>
                </div>
            </div>
        );
    }

}
export default connect(null, {setAppContext})(Home);