import React from 'react';
import {useDispatch} from 'react-redux';
import {toggleMenu} from './../redux/actions/index.js';

import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

export default function TopBar() {
    const dispatch = useDispatch();
    return (
        <div className="top-bar-container">
            <div className="logo-container">
                <h1><span>My</span>News</h1>
            </div>
            <div className="top-container">
                <IconButton
                    edge="start"
                    color="inherit"
                    aria-label="open drawer"
                    onClick={()=> dispatch(toggleMenu())}
                >
                    <MenuIcon />
                </IconButton>
            </div>
        </div>
    )
}