import React, {Component} from 'react';
import {connect} from 'react-redux';
import ContentByAppContext from './ContentByAppContext.js';
import {
    resetFiltersModalSubject,
    resetSourcesForResearchList, 
    resetSourcesListInModal,
    resetArticlesCategory, 
    resetCountry, 
    resetKeyword, 
    resetPageSize, 
    resetPageNumber, 
    resetArticlesResultsNumber, 
    setAppContext, 
    resetArticlesList,
    resetLanguage, 
    resetDateFrom, 
    resetDateTo} from './../redux/actions/index.js';

class Main extends Component {
    componentDidMount(){
        this.props.setAppContext(this.props.appContext);
        this.props.resetFiltersModalSubject();
        this.props.resetArticlesCategory();
        this.props.resetCountry();
        this.props.resetSourcesListInModal();
        this.props.resetSourcesForResearchList();
        this.props.resetKeyword();
        this.props.resetPageSize();
        this.props.resetPageNumber();
        this.props.resetArticlesList();
        this.props.resetArticlesResultsNumber();
        this.props.resetLanguage();
        this.props.resetDateFrom();
        this.props.resetDateTo();
    }
    render() {
        return (
            <div className="content-container">
                <ContentByAppContext/>
            </div>
        );
    }

}
export default connect(null, {
    resetFiltersModalSubject, 
    resetSourcesForResearchList, 
    resetSourcesListInModal,
    resetArticlesCategory, 
    resetCountry, 
    resetKeyword, 
    resetPageSize, 
    resetPageNumber, 
    resetArticlesResultsNumber, 
    setAppContext, 
    resetArticlesList,
    resetLanguage, 
    resetDateFrom, 
    resetDateTo})(Main);