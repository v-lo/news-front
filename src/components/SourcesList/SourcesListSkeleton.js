import React from "react";
import SourceItemSkeleton from "./../SourceItem/SourceItemSkeleton.js";
export default function SourcesListSkeleton() {
    const skeletonsNumberArray = [1,1,1,1,1, 1, 1, 1];
    return (
        <React.Fragment>
            {
                skeletonsNumberArray.map((skeleton,i) => (
                    <SourceItemSkeleton key={i} i={i}/>
                ))
            }
        </React.Fragment>
    );
}