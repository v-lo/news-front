import React from "react";
import SourceItem from "./../SourceItem/SourceItem.js";
import NoResults from './../Utils/NoResults.js';
import {useSelector} from 'react-redux';

export default function SourcesListResults() {
    const sourcesList = useSelector(state => state.sourcesList);
    return (
        sourcesList.length ? sourcesList.map((source, i) => (
            <SourceItem sourceDetails={source} key={i} i={i}/>
        ))
        :
        <NoResults/>
    );
}