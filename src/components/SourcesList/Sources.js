import React from "react";
import {useSelector} from 'react-redux';
import SourcesListResults from "./SourcesListResults.js";
import SourcesListSkeleton from "./SourcesListSkeleton.js";

export default function Sources() {
    const fetchStatus = useSelector(state => state.fetchStatus);
    return (
        <div className="source-list">
            {fetchStatus === "loading" && <SourcesListSkeleton/>}
            {fetchStatus === "ok" && <SourcesListResults/>}
        </div>
    );
}