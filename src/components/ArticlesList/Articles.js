import React from "react";
import {useSelector} from 'react-redux';
import ArticlesListResults from "./ArticlesListResults.js";
import ArticlesListSkeleton from "./ArticlesListSkeleton.js";

export default function ArticlesList() {
    const fetchStatus = useSelector(state => state.fetchStatus);
    return (
        <div className="article-list">
            {fetchStatus === "loading" && <ArticlesListSkeleton/>}
            {fetchStatus === "ok" && <ArticlesListResults/>}
        </div>
    );
}