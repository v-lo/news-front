import React from "react";
import ArticleItem from "./../ArticleItem/ArticleItem.js";
import NoResults from './../Utils/NoResults.js';
import {useSelector} from 'react-redux';

export default function ArticlesListResults() {
    const articlesList = useSelector(state => state.articlesList);
    return (
        articlesList.length ? articlesList.map((article, i) => (
            <ArticleItem articleDetails={article} key={i} i={i}/>
        ))
        :
        <NoResults/>
    );
}