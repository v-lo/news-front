import React from "react";
import ArticleItemSkeleton from "./../ArticleItem/ArticleItemSkeleton.js";
export default function ArticlesListSkeleton() {
    const skeletonsNumberArray = [1,1,1,1,1, 1, 1, 1];
    return (
        <React.Fragment>
            {
                skeletonsNumberArray.map((skeleton,i) => (
                    <ArticleItemSkeleton key={i} i={i}/>
                ))
            }
        </React.Fragment>
    );
}