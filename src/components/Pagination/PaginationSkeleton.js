import React from "react";
import Skeleton from "@material-ui/lab/Skeleton";

export default function PaginationSkeleton() {
  return (
        <div className="pagination-skeleton">
            <Skeleton disableAnimate={true} variant="rect" className="page-size" />
            <Skeleton disableAnimate={true} variant="rect" className="commands" />
        </div>
  );
}