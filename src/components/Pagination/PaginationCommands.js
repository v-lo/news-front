import React from "react";
import {useSelector, useDispatch} from 'react-redux';
import {goNextPage, goPreviousPage} from './../../redux/actions/index.js';
import Button from '@material-ui/core/Button';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';

export default function PaginationSize() {
    const dispatch = useDispatch();
    const articlesResultsNumber = useSelector(state => state.articlesResultsNumber);
    const pageNumber = useSelector(state => state.pageNumber);
    const pageSize = useSelector(state => state.pageSize);
    const totalNumberOfPages = Math.floor(articlesResultsNumber / pageSize) + 1;
    //Fct & control
    const canNextPage = (articlesResultsNumber > (pageSize * pageNumber));
    const goToNextPage = () =>{dispatch(goNextPage());}
    const canHandlePreviousPage = pageNumber > 1;
    const goToPreviousPage = () =>{dispatch(goPreviousPage());}
    return (
        <div className="pagination-commands-container">
            {
                canHandlePreviousPage && 
                <Button variant="contained" color="secondary" aria-label="previous page" onClick={goToPreviousPage}>
                    <NavigateBeforeIcon />
                </Button>
            }
            <p>Page {pageNumber} / {totalNumberOfPages}</p>
            {
                canNextPage &&
                <Button variant="contained" aria-label="next page" color="secondary" onClick={goToNextPage}>
                    <NavigateNextIcon />
                </Button>       
            }
        </div>
    );
}