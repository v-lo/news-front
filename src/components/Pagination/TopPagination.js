import React from "react";
import PaginationCommands from './PaginationCommands.js';
import PaginationSize from './PaginationSize.js';
import {useSelector} from 'react-redux';
import PaginationSkeleton from "./PaginationSkeleton.js";

export default function TopPagination() {
    const fetchStatus = useSelector(state => state.fetchStatus);
    const needPagination = useSelector(state => state.articlesResultsNumber)>0;
    return (
        <React.Fragment>
            {
                /* fetchStatus !== "error" &&  */
                <div className="all-pagination-container">
                    {
                        (fetchStatus === "ok" && needPagination) &&
                        <React.Fragment>
                            <PaginationSize/>
                            <PaginationCommands/>
                        </React.Fragment>
                    }{
                        fetchStatus === "loading" &&
                        <PaginationSkeleton/>
                    }
                </div>
            }
        </React.Fragment>
    );
}