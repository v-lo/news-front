import React from "react";
import {useSelector, useDispatch} from 'react-redux';
import {setPageSize, setPageNumber} from './../../redux/actions/index.js';
import Button from '@material-ui/core/Button';

export default function PaginationSize() {
    const dispatch = useDispatch();
    const pageSize = useSelector(state => state.pageSize);
    const twentyActive = pageSize === 20 && 'active';
    const fiftyActive = pageSize === 50 && 'active';
    const hundredActive = pageSize === 100 && 'active';
    const handlePageSize = (size) =>{
        dispatch(setPageSize(size));
        dispatch(setPageNumber(1));
    }
    return (
        <div className="pagination-size-container">
            <Button onClick={() => handlePageSize(20)} className={`${twentyActive}`}>20</Button>
            <Button onClick={() => handlePageSize(50)} className={`${fiftyActive}`}>50</Button>
            <Button onClick={() => handlePageSize(100)} className={`${hundredActive}`}>100</Button>
        </div>
    );
}