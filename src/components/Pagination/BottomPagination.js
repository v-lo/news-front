

import React from "react";
import PaginationCommands from './PaginationCommands.js';
import {useSelector} from 'react-redux';

export default function Pagination() {
    const fetchStatus = useSelector(state => state.fetchStatus);
    const needPagination = useSelector(state => state.articlesResultsNumber)>0;
    return (
        <React.Fragment>
            {
                fetchStatus !== "error" && 
                <React.Fragment>
                    {
                        fetchStatus === "ok" && needPagination &&
                        <div className="bottom-pagination-container">
                            <PaginationCommands/>
                        </div>
                    }
                </React.Fragment>
            }
        </React.Fragment>
    );
}