import React, { Component } from 'react';
import ModalError from './../Filters/Partials/ModalError.js';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import CircularProgress from '@material-ui/core/CircularProgress';
import {connect} from 'react-redux';
import {setSourcesListInModal, resetSourcesListInModal} from './../../redux/actions/index.js';
import {urlBase, apiKey} from './../../redux/actions/constants.js';

class FetchFilterSources extends Component {
    state={
        urlBase: urlBase,
        apiKey: apiKey,
        fetchStatus: "loading",
        errorCode : ""
    }
    componentDidMount(){
        this.fetchData();
    }
    fetchData = () => {
        this.setState({fetchStatus: "loading"})
        this.props.resetSourcesListInModal();
        const {urlBase, apiKey} = this.state;
        const urlBuilt = 
            urlBase 
            + "sources?"+
            "&apiKey=" + apiKey;
        fetch(urlBuilt)
            .then(data => data.json())
            .then(data => {
                this.fetchSuccess(data);
            },
            error => {
                alert("Veuillez nous excuser, une erreur est survenue lors de l'appel à l'API NewsAPI.");
            }
        );
    }
    fetchSuccess = (apiData) =>{
        const status = apiData.status;
        if (status === "ok"){
            this.setState({fetchStatus : "loading"});
            const sources = apiData.sources;
            this.props.setSourcesListInModal(sources);
        } else {
            this.setState({fetchStatus : "error", errorCode: apiData.code});
        }
    }
    render() {
        const {fetchStatus, errorCode} = this.state;
        return (
            <DialogContent>
                {fetchStatus ==="error" &&
                    <React.Fragment>
                        <DialogContentText>
                            <ModalError errorCode={errorCode}/>
                        </DialogContentText>
                    </React.Fragment>
                }
                {fetchStatus === "loading" &&
                    <React.Fragment>
                        <DialogContentText>
                            Récupération des sources disponibles en cours.
                        </DialogContentText>
                        <div className="sources-list-loader">
                            <CircularProgress className="loading-circular-center"/>
                        </div>
                    </React.Fragment>
                }
            </DialogContent>
        )}
}
export default connect(null, {setSourcesListInModal, resetSourcesListInModal})(FetchFilterSources);