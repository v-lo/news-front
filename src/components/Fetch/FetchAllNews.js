import React, { Component } from 'react';
import Articles from './../ArticlesList/Articles.js';
import Error from './../Errors/Error.js';
import {connect} from 'react-redux';
import {resetArticlesList,resetArticlesResultsNumber, setArticlesResultsNumber, setArticlesList, setFetchStatus, setErrorCode, resetPageNumber} from './../../redux/actions/index.js';
import {urlBase, apiKey} from './../../redux/actions/constants.js';

class FetchAllNews extends Component {
    state={
        urlBase: urlBase,
        apiKey: apiKey,
        appContext : this.props.appContext,
        dateFrom : this.props.dateFrom,
        dateTo : this.props.dateTo,
        keyword:this.props.keyword,
        language:this.props.language,
        sourcesSelectedList: this.props.sourcesSelectedList,
        pageNumber : this.props.pageNumber,
        pageSize : this.props.pageSize,
        fetchStatus: this.props.fetchStatus
    }
    componentDidUpdate(prevProps, prevState){
        if (
            this.props.dateFrom !== prevProps.dateFrom || 
            this.props.language !== prevProps.language ||
            this.props.dateTo !== prevProps.dateTo || 
            this.props.keyword !== prevProps.keyword || 
            this.props.sourcesSelectedList !== prevProps.sourcesSelectedList
        ){
            this.props.resetPageNumber();
            this.fetchData();
        } else if (
            this.props.pageNumber !== prevProps.pageNumber ||
            this.props.pageSize !== prevProps.pageSize)
        {
            this.fetchData();
        }
    }
    static getDerivedStateFromProps(props, state) {
        if(
            props.pageSize !== state.pageSize || 
            props.keyword !== state.keyword || 
            props.language !== state.language ||
            props.dateFrom !== state.dateFrom ||
            props.dateTo !== state.dateTo ||
            props.sourcesSelectedList !== state.sourcesSelectedList ||
            props.pageNumber !== state.pageNumber ||
            props.fetchStatus !== state.fetchStatus){
            return {
                pageSize : props.pageSize,
                language: props.language,
                dateFrom: props.dateFrom,
                dateTo: props.dateTo,
                keyword: props.keyword,
                sourcesSelectedList: props.sourcesSelectedList,
                pageNumber: props.pageNumber,
                fetchStatus: props.fetchStatus
            };
        }  else {
            return null;
        }
    }
    fetchData = () => {
        this.props.setFetchStatus("loading");
        this.props.resetArticlesList();
        this.props.resetArticlesResultsNumber();
        const {urlBase, appContext, keyword, sourcesSelectedList, language, dateFrom, dateTo, pageSize, pageNumber,apiKey} = this.state;
        const urlBuilt = 
            urlBase 
            + appContext.fetchUrl + 
            "sources=" + sourcesSelectedList +
            "&from="+ dateFrom.formated + 
            "&to="+ dateTo.formated + 
            "&q=" + keyword +
            "&language=" + language +
            "&page=" + pageNumber +
            "&pageSize=" + pageSize +
            "&apiKey=" + apiKey;
            //console.log(url)
        fetch(urlBuilt)
            .then(data => data.json())
            .then(data => {
                this.fetchSuccess(data);
            },
            error => {
                this.props.setFetchStatus("error");
                this.props.setErrorCode("unexpectedError");
            }
        );
    }
    fetchSuccess = (apiData) =>{
        const status = apiData.status;
        this.props.setFetchStatus(status);
        if (status === "ok"){
            const articles = apiData.articles;
            const articlesResultsNumber = apiData.totalResults;
            this.props.setFetchStatus(status);
            this.props.setArticlesList(articles);
            this.props.setArticlesResultsNumber(articlesResultsNumber);
        } else {
            const errorCode = apiData.code;
            this.props.setErrorCode(errorCode);
        }
    }
    render() {
        const {fetchStatus} = this.state;
        return (
            <React.Fragment>
                {fetchStatus !=="error" ? <Articles/> : <Error/>}
            </React.Fragment>
        )}
    }
const mapStateToProps = state => {
    const {pageSize, pageNumber, articlesCategory, appContext, articlesList, fetchStatus, country, keyword, sourcesSelectedList, dateFrom, dateTo, language} = state;
    return { pageSize, pageNumber, articlesCategory, appContext, articlesList, fetchStatus, country, keyword, sourcesSelectedList, dateFrom, dateTo, language};
};
export default connect(mapStateToProps, {resetArticlesList, setArticlesList,resetArticlesResultsNumber, setArticlesResultsNumber, setFetchStatus, setErrorCode, resetPageNumber})(FetchAllNews);