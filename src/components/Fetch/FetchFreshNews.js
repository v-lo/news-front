import React, { Component } from 'react';
import Articles from './../ArticlesList/Articles.js';
import Error from './../Errors/Error.js';
import {connect} from 'react-redux';
import {resetArticlesList, resetArticlesResultsNumber, setArticlesResultsNumber, setArticlesList, setFetchStatus, setErrorCode, resetPageNumber} from './../../redux/actions/index.js';
import {urlBase, apiKey} from './../../redux/actions/constants.js';

class FetchFreshNews extends Component {
    state={
        urlBase: urlBase,
        apiKey: apiKey,
        appContext : this.props.appContext,
        articlesCategory : this.props.articlesCategory,
        country: this.props.country,
        keyword:this.props.keyword,
        sourcesSelectedList: this.props.sourcesSelectedList,
        pageNumber : this.props.pageNumber,
        pageSize : this.props.pageSize,
        fetchStatus: this.props.fetchStatus
    }
    componentDidUpdate(prevProps, prevState){
        if (
            this.props.country !== prevProps.country || 
            this.props.keyword !== prevProps.keyword || 
            this.props.articlesCategory !== prevProps.articlesCategory ||
            this.props.sourcesSelectedList !== prevProps.sourcesSelectedList
        ){
            this.props.resetPageNumber();
            this.fetchData();
        } else if (
            this.props.pageNumber !== prevProps.pageNumber ||
            this.props.pageSize !== prevProps.pageSize)
        {
            this.fetchData();
        }
    }
    static getDerivedStateFromProps(props, state) {
        if(props.country !== state.country || 
            props.pageSize !== state.pageSize || 
            props.keyword !== state.keyword || 
            props.articlesCategory !== state.articlesCategory ||
            props.sourcesSelectedList !== state.sourcesSelectedList ||
            props.pageNumber !== state.pageNumber ||
            props.fetchStatus !== state.fetchStatus){
            return {
                pageSize : props.pageSize,
                country: props.country,
                keyword: props.keyword,
                articlesCategory: props.articlesCategory,
                sourcesSelectedList: props.sourcesSelectedList,
                pageNumber: props.pageNumber,
                fetchStatus: props.fetchStatus
            };
        } else{
            return null;
        }
    }
    fetchData = () => {
        this.props.setFetchStatus("loading");
        this.props.resetArticlesList();
        this.props.resetArticlesResultsNumber();
        const {sourcesSelectedList, urlBase, appContext, articlesCategory, country, keyword, pageSize, pageNumber,apiKey} = this.state;
        const urlSourceBuilt = sourcesSelectedList.join(',');
        const sourcesCatCountryUrl = this.manipulateSourcesAndCategories(urlSourceBuilt, articlesCategory, country);
        const urlBuilt = 
            urlBase 
            + appContext.fetchUrl + 
            sourcesCatCountryUrl +
            "&q=" + keyword +
            "&page=" + pageNumber +
            "&pageSize=" + pageSize +
            "&apiKey=" + apiKey;
        fetch(urlBuilt)
            .then(data => data.json())
            .then(data => {
                this.fetchSuccess(data);
            },
            error => {
                this.props.setFetchStatus("error");
                this.props.setErrorCode("unexpectedError");
            }
        );
    }
    manipulateSourcesAndCategories = (source, cat, country) =>{
        let url = "country=";
        if(source){
            url = url + "&sources=" +source;
        } else if (cat.fetchUrl.length> 0 || country.length > 0){
            if(country.length >0){
                url = url  + country;
            }
            if (cat.fetchUrl.length> 0){
                url = url + "&category=" + cat.fetchUrl;
            }
        } else{
            return "country=";
        }
        return url;
    }
    fetchSuccess = (apiData) =>{
        const status = apiData.status;
        this.props.setFetchStatus(status);
        if (status === "ok"){
            const articles = apiData.articles;
            const articlesResultsNumber = apiData.totalResults;
            this.props.setFetchStatus(status);
            this.props.setArticlesList(articles);
            this.props.setArticlesResultsNumber(articlesResultsNumber);
        } else {
            const errorCode = apiData.code;
            this.props.setErrorCode(errorCode);
        }
    }
    render() {
        const {fetchStatus} = this.state;
        return (
            <React.Fragment>
                {fetchStatus !=="error" ? <Articles/> : <Error/>}
            </React.Fragment>
        )}
    }
const mapStateToProps = state => {
    const {pageSize, pageNumber, articlesCategory, appContext, articlesList, fetchStatus, country, keyword, sourcesSelectedList} = state;
    return { pageSize, pageNumber, articlesCategory, appContext, articlesList, fetchStatus, country, keyword, sourcesSelectedList};
};
export default connect(mapStateToProps, {resetArticlesList, setArticlesList, resetArticlesResultsNumber, setArticlesResultsNumber, setFetchStatus, setErrorCode, resetPageNumber})(FetchFreshNews);