import React, { Component } from 'react';
import Sources from './../SourcesList/Sources.js';
import Error from './../Errors/Error.js';
import {connect} from 'react-redux';
import {setSourcesList, resetSourcesList, setFetchStatus, setErrorCode} from './../../redux/actions/index.js';
import {urlBase, apiKey} from './../../redux/actions/constants.js';

class FetchSources extends Component {
    state={
        urlBase: urlBase,
        apiKey: apiKey,
        appContext : this.props.appContext,
        articlesCategory : this.props.articlesCategory,
        country: this.props.country,
        language: this.props.language,
        fetchStatus: this.props.fetchStatus
    }
    componentDidMount(){
        this.fetchData();
    }
    componentDidUpdate(prevProps, prevState){
        if (
            this.props.country !== prevProps.country || 
            this.props.language !== prevProps.language || 
            this.props.articlesCategory !== prevProps.articlesCategory
        ){
            this.fetchData();
        }
    }
    static getDerivedStateFromProps(props, state) {
        if(props.country !== state.country || 
            props.language !== state.language || 
            props.articlesCategory !== state.articlesCategory ||
            props.fetchStatus !== state.fetchStatus){
            return {
                language : props.language,
                country: props.country,
                articlesCategory: props.articlesCategory,
                fetchStatus: props.fetchStatus
            };
        }  else {
            return null;
        }
    }
    //call build and execute request to news API
    fetchData = () => {
        this.props.setFetchStatus("loading");
        this.props.resetSourcesList();
        const {urlBase, appContext, articlesCategory, language, country, apiKey} = this.state;
        const urlBuilt = 
            urlBase 
            + appContext.fetchUrl + 
            "country=" + country +
            "&category=" + articlesCategory.fetchUrl + 
            "&language=" + language +
            "&apiKey=" + apiKey;
            //console.log(url)
        fetch(urlBuilt)
            .then(data => data.json())
            .then(data => {
                this.fetchSuccess(data);
            },
            error => {
                alert("Veuillez nous excuser, une erreur est survenue lors de l'appel à l'API NewsAPI.");
            }
        );
    }
    //Manipulate data returned by the API
    fetchSuccess = (apiData) =>{
        const status = apiData.status;
        this.props.setFetchStatus(status);
        if (status === "ok"){
            const sources = apiData.sources;
            this.props.setFetchStatus(status);
            this.props.setSourcesList(sources);
        } else {
            const errorCode = apiData.code;
            this.props.setErrorCode(errorCode);
        }
    }
    render() {
        const {fetchStatus} = this.state;
        return (
            <React.Fragment>
                {fetchStatus !=="error" ? 
                    <Sources/>
                    :
                    <Error/>
                }
            </React.Fragment>
        )}
    }
const mapStateToProps = state => {
    const {articlesCategory, appContext, sourcesList, fetchStatus, country, language} = state;
    return {articlesCategory, appContext, sourcesList, fetchStatus, country, language};
};
export default connect(mapStateToProps, {setSourcesList, resetSourcesList, setFetchStatus, setErrorCode})(FetchSources);