import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper';
import LinkIcon from '@material-ui/icons/Link';
import placeholder from './../../assets/img/image-placeholder.png';
import Button from '@material-ui/core/Button';
import Grow from '@material-ui/core/Grow';
import Moment from 'react-moment';
import 'moment/locale/fr';

class ArticleItem extends Component {
    _isMounted = false;
    timer= false;
    state={
        growed:false,
        i: this.props.i
    }
    componentDidMount(){
        this._isMounted = true;
        this.timer = setTimeout(() => { this.setState({ growed: !this.state.growed }) }, 40 * this.state.i );
    }
    componentWillUnmount() {
        this._isMounted = false;
        clearTimeout(this.timer);
    }
    render() {
        const growed = this.state.growed;
        const article = this.props.articleDetails;
        const posterStyle = {background:"url(" + this.props.articleDetails.urlToImage + ") center center no-repeat"};
        const altPosterStyle = {background:"url(" + placeholder + ") center center no-repeat"};
        return (
            <Grow in={growed} timeout={250}>
                <Paper className="article-item flex-section">
                    <div className="article-poster-container" style={altPosterStyle}>
                        <div className="article-poster" style={posterStyle}></div>
                    </div>
                    <div className="article-content">
                        <div className="article-infos">
                            <p className="source">{article.source.name}</p>
                            <p className="date"><Moment fromNow>{article.publishedAt}</Moment></p>
                        </div>
                        <div className="chapo">
                            <h3>{article.title}</h3>
                            <p className="article-text">{article.description}</p>
                        </div>
                        <Button href={article.url} target="_blank" size="small" className="article-action-button">
                            Lire
                            <LinkIcon/>
                        </Button>
                    </div>
                </Paper>
            </Grow>
        )}
    }

export default ArticleItem;