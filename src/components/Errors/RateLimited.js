import React, { Component } from 'react';
import Icon from '@material-ui/core/Icon';

class RateLimited extends Component {
    render() {
        return (
            <div className="error-container">
                <Icon>sentiment_very_dissatisfied</Icon>
                <p>L'API a limité le taux de requête pour ce compte.</p>
            </div>
        )}
}
export default RateLimited;