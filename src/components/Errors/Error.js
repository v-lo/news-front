import React from "react";
import {useSelector} from 'react-redux';
import ApiKeyDisabled from './ApiKeyDisabled.js';
import ApiKeyExhausted from './ApiKeyExhausted.js';
import ApiKeyInvalid from './ApiKeyInvalid.js';
import ApiKeyMissing from './ApiKeyMissing.js';
import ParameterInvalid from './ParameterInvalid.js';
import ParametersMissing from './ParametersMissing.js';
import RateLimited from './RateLimited.js';
import SourceDoesNotExist from './SourceDoesNotExist.js';
import SourcesTooMany from './SourcesTooMany.js';
import UnexpectedError from './UnexpectedError.js';
import MaximumResultsReached from './MaximumResultsReached.js';
import ParametersIncompatible from './ParametersIncompatible.js';

export default function Error() {
    const errorCode = useSelector(state => state.errorCode);
    return (
        <React.Fragment>
            {errorCode === "apiKeyDisabled" && <ApiKeyDisabled/>}
            {errorCode === "apiKeyExhausted" && <ApiKeyExhausted/>}
            {errorCode === "apiKeyInvalid" && <ApiKeyInvalid/>}
            {errorCode === "apiKeyMissing" && <ApiKeyMissing/>}
            {errorCode === "parametersMissing" && <ParametersMissing/>}
            {errorCode === "parameterInvalid" && <ParameterInvalid/>}
            {errorCode === "rateLimited" && <RateLimited/>}
            {errorCode === "sourcesTooMany" && <SourcesTooMany/>}
            {errorCode === "sourceDoesNotExist" && <SourceDoesNotExist/>}
            {errorCode === "maximumResultsReached" && <MaximumResultsReached/>}
            {errorCode === "unexpectedError" && <UnexpectedError/>}
            {errorCode === "parametersIncompatible" && <ParametersIncompatible/>}
        </React.Fragment>
    );
}