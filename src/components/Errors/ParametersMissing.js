import React, { Component } from 'react';
import Icon from '@material-ui/core/Icon';

class ParametersMissing extends Component {
    render() {
        return (
            <div className="error-container">
                <Icon>add</Icon>
                <p>Ajoutez des filtres via les boutons en haut de page.</p>
            </div>
        )}
}
export default ParametersMissing;