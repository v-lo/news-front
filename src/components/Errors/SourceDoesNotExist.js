import React, { Component } from 'react';
import Icon from '@material-ui/core/Icon';

class SourceDoesNotExist extends Component {
    render() {
        return (
            <div className="error-container">
                <Icon>sentiment_very_dissatisfied</Icon>
                <p>La source demandée n'existe pas.</p>
            </div>
        )}
}
export default SourceDoesNotExist;