import React, { Component } from 'react';
import Icon from '@material-ui/core/Icon';

class ParametersIncompatible extends Component {
    render() {
        return (
            <div className="error-container">
                <Icon>sentiment_very_dissatisfied</Icon>
                <p>Les filtres de recherche ne sont pas compatibles.</p>
            </div>
        )}
}
export default ParametersIncompatible;