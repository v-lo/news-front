import React, { Component } from 'react';
import Icon from '@material-ui/core/Icon';

class ApiKeyMissing extends Component {
    render() {
        return (
            <div className="error-container">
                <Icon>sentiment_very_dissatisfied</Icon>
                <p>La clé d'API est manquante lors de la requète.</p>
            </div>
        )}
}
export default ApiKeyMissing;