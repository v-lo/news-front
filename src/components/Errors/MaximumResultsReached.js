import React, { Component } from 'react';
import Icon from '@material-ui/core/Icon';

class MaximumResultsReached extends Component {
    render() {
        return (
            <div className="error-container">
                <Icon>sentiment_very_dissatisfied</Icon>
                <p>La formule gratuite utilisée par le développeur ne permet pas d'accéder à ces résultats (>100).</p>
            </div>
        )}
}
export default MaximumResultsReached;