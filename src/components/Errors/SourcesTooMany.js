import React, { Component } from 'react';
import Icon from '@material-ui/core/Icon';

class SourcesTooMany extends Component {
    render() {
        return (
            <div className="error-container">
                <Icon>sentiment_very_dissatisfied</Icon>
                <p>Vous avez inclus trop de sources différentes dans la requète.</p>
            </div>
        )}
}
export default SourcesTooMany;