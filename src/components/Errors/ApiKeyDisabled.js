import React, { Component } from 'react';
import Icon from '@material-ui/core/Icon';

class ApiKeyDisabled extends Component {
    render() {
        return (
            <div className="error-container">
                <Icon>sentiment_very_dissatisfied</Icon>
                <p>La clé d'API a été désactivée.</p>
            </div>
        )}
}
export default ApiKeyDisabled;