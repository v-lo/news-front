import React, { Component } from 'react';
import Icon from '@material-ui/core/Icon';

class ApiKeyInvalid extends Component {
    render() {
        return (
            <div className="error-container">
                <Icon>sentiment_very_dissatisfied</Icon>
                <p>La clé d'API n'est pas valide.</p>
            </div>
        )}
}
export default ApiKeyInvalid;