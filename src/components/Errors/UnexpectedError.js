import React, { Component } from 'react';
import Icon from '@material-ui/core/Icon';

class NoResults extends Component {
    render() {
        return (
            <div className="error-container">
                <Icon>sentiment_very_dissatisfied</Icon>
                <p>Un problème est survenu lors de l'appel à l'API.</p>
            </div>
        )}
}
export default NoResults;