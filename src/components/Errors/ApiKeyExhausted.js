import React, { Component } from 'react';
import Icon from '@material-ui/core/Icon';

class ApiKeyExhausted extends Component {
    render() {
        return (
            <div className="error-container">
                <Icon>sentiment_very_dissatisfied</Icon>
                <p>Trop d'appels ont été effectués avec cett clé d'API.</p>
            </div>
        )}
}
export default ApiKeyExhausted;