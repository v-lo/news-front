import React, { Component } from 'react';
import Icon from '@material-ui/core/Icon';

class ParameterInvalid extends Component {
    render() {
        return (
            <div className="error-container">
                <Icon>sentiment_very_dissatisfied</Icon>
                <p>L'un des paramètres de la requète n'est pas supporté.</p>
            </div>
        )}
}
export default ParameterInvalid;