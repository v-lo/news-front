import React from "react";
import Icon from '@material-ui/core/Icon';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import {useSelector, useDispatch} from 'react-redux';
import {resetArticlesCategory, openFiltersModal, setFiltersModalSubject} from './../../../redux/actions/index.js';

export default function ArticlesCategoryChip() {
    const dispatch = useDispatch();
    const articlesCategory = useSelector(state => state.articlesCategory);
    const openFilterModal = (filterSubject) => {
        dispatch(setFiltersModalSubject(filterSubject));
        dispatch(openFiltersModal());
    };
    return (
        <Chip
            avatar={<Avatar><Icon>{articlesCategory.icone}</Icon></Avatar>}
            label={articlesCategory.label}
            className="chip-item changeable"
            onDelete={() => dispatch(resetArticlesCategory())} 
            onClick={()=>{openFilterModal('Catégorie')}}
        />
    );
}