import React from "react";
import Icon from '@material-ui/core/Icon';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import {useSelector, useDispatch} from 'react-redux';
import {resetSourcesForResearchList, openFiltersModal, setFiltersModalSubject} from './../../../redux/actions/index.js';

export default function SourceChip() {
    const dispatch = useDispatch();
    const sourcesList = useSelector(state => state.sourcesSelectedList)
    const openFilterModal = (filterSubject) => {
        dispatch(setFiltersModalSubject(filterSubject));
        dispatch(openFiltersModal());
    };
    const sourcesLabel = sourcesList.length;
    const multipleResults = sourcesList.length > 1 ? 's' : '';
    return (
            <Chip
                avatar={<Avatar><Icon>supervisor_account</Icon></Avatar>}
                label={sourcesLabel+' source' + multipleResults}
                className="chip-item changeable"
                onDelete={() => dispatch(resetSourcesForResearchList())}
                onClick={()=>{openFilterModal('Sources')}}
            />
    );
}