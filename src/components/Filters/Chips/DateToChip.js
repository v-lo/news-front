import React from "react";
import Icon from '@material-ui/core/Icon';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import {useSelector, useDispatch} from 'react-redux';
import {resetDateTo, openFiltersModal, setFiltersModalSubject} from './../../../redux/actions/index.js';

export default function DateToChip() {
    const dispatch = useDispatch();
    const dateToLabel = useSelector(state => state.dateTo.stringified);
    const openFilterModal = (filterSubject) => {
        dispatch(setFiltersModalSubject(filterSubject));
        dispatch(openFiltersModal());
    };
    return (
        <Chip
            avatar={<Avatar><Icon>update</Icon></Avatar>}
            label={dateToLabel}
            className="chip-item changeable"
            onDelete={() => dispatch(resetDateTo())} 
            onClick={()=>{openFilterModal('Date minimale')}}
        />
    );
};