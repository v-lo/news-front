import React from "react";
import Icon from '@material-ui/core/Icon';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import {useSelector, useDispatch} from 'react-redux';
import {resetLanguage, openFiltersModal, setFiltersModalSubject} from './../../../redux/actions/index.js';

export default function LanguageChip() {
    const dispatch = useDispatch();
    const language = useSelector(state => state.language);
    const openFilterModal = (filterSubject) => {
        dispatch(setFiltersModalSubject(filterSubject));
        dispatch(openFiltersModal());
    };
    return (
        <Chip
            avatar={<Avatar><Icon>translate</Icon></Avatar>}
            label={language}
            className="chip-item changeable"
            onDelete={() => dispatch(resetLanguage())}
            onClick={()=>{openFilterModal('Langue')}}
        />
    );
}