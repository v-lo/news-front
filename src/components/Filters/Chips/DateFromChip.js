import React from "react";
import Icon from '@material-ui/core/Icon';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import {useSelector, useDispatch} from 'react-redux';
import {resetDateFrom, openFiltersModal, setFiltersModalSubject} from './../../../redux/actions/index.js';

export default function DateFromChip() {
    const dispatch = useDispatch();
    const dateFromLabel = useSelector(state => state.dateFrom.stringified);
    const openFilterModal = (filterSubject) => {
        dispatch(setFiltersModalSubject(filterSubject));
        dispatch(openFiltersModal());
    };
    return (
        <Chip
            avatar={<Avatar><Icon>history</Icon></Avatar>}
            label={dateFromLabel}
            className="chip-item changeable"
            onDelete={() => dispatch(resetDateFrom())} 
            onClick={()=>{openFilterModal('Date minimale')}}
        />
    );
};