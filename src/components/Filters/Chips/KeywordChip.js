import React from "react";
import Icon from '@material-ui/core/Icon';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import {useSelector, useDispatch} from 'react-redux';
import {resetKeyword, openFiltersModal, setFiltersModalSubject} from './../../../redux/actions/index.js';

export default function KeywordChip() {
    const dispatch = useDispatch();
    const keyword = useSelector(state => state.keyword);
    const openFilterModal = (filterSubject) => {
        dispatch(setFiltersModalSubject(filterSubject));
        dispatch(openFiltersModal());
    };
    return (
        <Chip
            avatar={<Avatar><Icon>format_quote</Icon></Avatar>}
            label={keyword}
            className="chip-item changeable"
            onDelete={() => dispatch(resetKeyword())} 
            onClick={()=>{openFilterModal('Mot-clés')}}
        />
    );
}