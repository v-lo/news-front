import React from "react";
import Icon from '@material-ui/core/Icon';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import {useSelector, useDispatch} from 'react-redux';
import {resetCountry, openFiltersModal, setFiltersModalSubject} from './../../../redux/actions/index.js';

export default function CountryChip() {
    const dispatch = useDispatch();
    const country = useSelector(state => state.country);
    const openFilterModal = (filterSubject) => {
        dispatch(setFiltersModalSubject(filterSubject));
        dispatch(openFiltersModal());
    };
    return (
        <Chip
            avatar={<Avatar><Icon>language</Icon></Avatar>}
            label={country}
            className="chip-item changeable"
            onDelete={() => dispatch(resetCountry())}
            onClick={()=>{openFilterModal('Pays')}}
        />
    );
}