import React from "react";
import KeywordModal from './Modals/KeywordModal.js';
import CountryModal from './Modals/CountryModal.js';
import ArticlesCategoryModal from './Modals/ArticlesCategoryModal.js';
import LanguageModal from './Modals/LanguageModal.js';
import DateFromModal from './Modals/DateFromModal.js';
import DateToModal from './Modals/DateToModal.js';
import SourcesModal from './Modals/SourcesModal.js';
import Dialog from '@material-ui/core/Dialog';
import {useSelector, useDispatch} from 'react-redux';
import {closeFiltersModal} from './../../redux/actions/index.js';

export default function FiltersModal() {
    const dispatch = useDispatch();
    const isFiltersModalOpen = useSelector(state => state.isFiltersModalOpen);
    const filtersModalSubject = useSelector(state => state.filtersModalSubject);
    return (
        
        isFiltersModalOpen && 
            <Dialog aria-labelledby="simple-dialog-title" open={isFiltersModalOpen} onClose={() => dispatch(closeFiltersModal())}>
                {filtersModalSubject === "Mot-clés" && <KeywordModal/>}
                {filtersModalSubject === "Pays" && <CountryModal/>}
                {filtersModalSubject === "Catégorie" && <ArticlesCategoryModal/>}
                {filtersModalSubject === "Langue" && <LanguageModal/>}
                {filtersModalSubject === "Date minimale" && <DateFromModal/>}
                {filtersModalSubject === "Date maximale" && <DateToModal/>}
                {filtersModalSubject === "Sources" && <SourcesModal/>}
            </Dialog>
    );
}