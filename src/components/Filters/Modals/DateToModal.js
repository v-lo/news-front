import React, {useState} from "react";
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import DateFnsUtils from '@date-io/date-fns';
import frLocale from "date-fns/locale/fr";
import {MuiPickersUtilsProvider, KeyboardDatePicker,} from '@material-ui/pickers';
import Button from '@material-ui/core/Button';
import {useDispatch} from 'react-redux';
import {closeFiltersModal, setDateTo, resetDateTo} from './../../../redux/actions/index.js';

export default function DateToModal() {
    const dispatch = useDispatch();
    const [dateToFieldsetValue, setDateToFieldsetValue] = useState(new Date('2019-10-18T21:11:54'));
    const validateDateTo = () =>{
        dispatch(setDateTo(dateToFieldsetValue));
        dispatch(closeFiltersModal());
    };
    const choseNoDateTo = () =>{
        dispatch(resetDateTo());
        dispatch(closeFiltersModal());
    };
    const handleDateToFielsetValue = (date) => {
        setDateToFieldsetValue(date);
    }
    return (
        <React.Fragment>
            <DialogTitle>Date maximale</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Choisissez la date maximale jusqu'à laquelle les articles doivent être recherchés.
                </DialogContentText>
                <MuiPickersUtilsProvider utils={DateFnsUtils} locale={frLocale}>
                    <KeyboardDatePicker
                        disableToolbar
                        variant="inline"
                        format="dd/MM/yyyy"
                        margin="normal"
                        id="date-picker-inline"
                        label="Date maximale"
                        value={dateToFieldsetValue}
                        onChange={handleDateToFielsetValue}
                        KeyboardButtonProps={{'aria-label': 'change date',}}
                    />
                </MuiPickersUtilsProvider>
            </DialogContent>
            <DialogActions className="actions-container">
                <Button onClick={choseNoDateTo} className="cta">
                    Pas de date maximale
                </Button>
                <Button onClick={validateDateTo} className="important-cta">
                    Valider
                </Button>
            </DialogActions>
        </React.Fragment>
    );
}