import React from "react";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';

import {categoriesList} from './../../../redux/actions/constants.js';
import {useSelector, useDispatch} from 'react-redux';
import {closeFiltersModal, setArticlesCategory, resetArticlesCategory} from './../../../redux/actions/index.js';

export default function ArticlesCategoryModal() {
    const dispatch = useDispatch();
    const categories = categoriesList;
    const ArticlesCategorySelected = useSelector(state => state.articlesCategory);
    const handleArticlesCategory = (category) =>{
        dispatch(setArticlesCategory(category));
        dispatch(closeFiltersModal());
    };
    const choseNoArticlesCategory = () =>{
        dispatch(resetArticlesCategory());
        dispatch(closeFiltersModal());
    };
    return (
        <React.Fragment>
            <DialogTitle>Catégories</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Choisissez une catégorie spécifique pour la recherche
                </DialogContentText>
                <RadioGroup aria-label="subcat" name="subcat" value={ArticlesCategorySelected}>
                    {categories.map(cat =>(
                        <FormControlLabel value={cat} control={<Radio />} label={cat.label} key={cat.label} onClick={() => handleArticlesCategory(cat)}/>
                    ))
                    }
                </RadioGroup>
            </DialogContent>
            <DialogActions className="actions-container">
                <Button onClick={choseNoArticlesCategory} className="cta">
                    Pas de catégorie
                </Button>
            </DialogActions>
        </React.Fragment>
    );
}