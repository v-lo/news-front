import React, {useState} from "react";
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import {useDispatch} from 'react-redux';
import {closeFiltersModal, setKeyword, resetKeyword} from './../../../redux/actions/index.js';

export default function KeywordModal() {
    const dispatch = useDispatch();
    const [keywordFieldsetValue, setKeywordFieldsetValue] = useState("");
    const validateKeyword = () =>{
        dispatch(setKeyword(keywordFieldsetValue));
        dispatch(closeFiltersModal());
    };
    const choseNoKeyword = () =>{
        dispatch(resetKeyword());
        dispatch(closeFiltersModal());
    };
    const handleKeywordFielsetValue = (e) => {
        setKeywordFieldsetValue(e.target.value);
    };
    return (
        <div>
            <DialogTitle>Mots clés</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Ajoutez un texte à rechercher
                </DialogContentText>
                <TextField
                    autoFocus
                    id="search"
                    label="rechercher"
                    defaultValue={keywordFieldsetValue}
                    type="text"
                    fullWidth
                    onChange={handleKeywordFielsetValue}
                    onKeyPress={event => {
                        if (event.key === "Enter") {
                            validateKeyword();
                        }
                    }}
                />
            </DialogContent>
            <DialogActions className="actions-container">
                <Button onClick={choseNoKeyword} className="cta">
                    Pas de mots-clés
                </Button>
                <Button onClick={validateKeyword} className="important-cta">
                    Valider
                </Button>
            </DialogActions>
        </div>
    );
}