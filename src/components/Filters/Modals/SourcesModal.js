import React from "react";
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import {useSelector, useDispatch} from 'react-redux';
import {closeFiltersModal, setSourcesForResearchList, resetSourcesForResearchList, resetSourcesListInModal} from './../../../redux/actions/index.js';
import FetchFilterSources from "../../Fetch/FetchFilterSources.js";

export default function LanguageModal() {
    const dispatch = useDispatch();
    const availableSourcesList = useSelector(state => state.sourcesListInModal);
    let sourcesNewSelection = [];
    const handleSourcesSelected = (e) =>{
            let alreadySet = false;
            for (let i = 0; i<sourcesNewSelection.length; i++){
                if (sourcesNewSelection[i] === e.target.value){
                    sourcesNewSelection.filter(id => id ===  e.target.value);
                    sourcesNewSelection.splice(i, 1); 
                    alreadySet= true;
                }
            }
            !alreadySet && sourcesNewSelection.push((e.target.value));
    };
    const choseNoSources = () =>{
        dispatch(resetSourcesListInModal());
        dispatch(resetSourcesForResearchList());
        dispatch(closeFiltersModal());
    };
    const validateSources = ()=>{
        dispatch(resetSourcesListInModal());
        dispatch(setSourcesForResearchList(sourcesNewSelection));
        dispatch(closeFiltersModal());
    }
    return (
        <React.Fragment>
            <DialogTitle>Sources</DialogTitle>
            { availableSourcesList.length === 0 &&
                <FetchFilterSources/>
            }
            {availableSourcesList.length> 0 && 
                <DialogContent>
                    <DialogContentText>
                        Choisissez les sources pour lesquelles vous souhaitez rechercher des articles.
                    </DialogContentText>
                    <FormControl component="fieldset" className="sources-list-checkbox-list">
                        <FormGroup value={''} >
                            {availableSourcesList.map((source, i) => (
                                <FormControlLabel
                                    key={i}
                                    control={<Checkbox onChange={handleSourcesSelected} /* checked={isSourceSelected(source.id)} */ value={source.id}/>}
                                    label={source.name} 
                                />
                            ))}
                        </FormGroup>
                    </FormControl>
                </DialogContent>
            }
            <DialogActions className="actions-container">
                <Button onClick={choseNoSources} className="cta">
                    Pas de sources
                </Button>
                <Button onClick={validateSources} className="important-cta">
                    Valider
                </Button>
            </DialogActions>
        </React.Fragment>
    );
}