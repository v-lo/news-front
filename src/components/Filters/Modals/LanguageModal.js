import React from "react";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import {availableLanguages} from './../../../redux/actions/constants.js';
import {useSelector, useDispatch} from 'react-redux';
import {closeFiltersModal, setLanguage, resetLanguage} from './../../../redux/actions/index.js';

export default function LanguageModal() {
    const dispatch = useDispatch();
    const languageFilterValue = useSelector(state => state.language);
    const languageList = availableLanguages;
    const handleLanguage = (e) =>{
        dispatch(setLanguage(e.target.value));
        dispatch(closeFiltersModal());
    };
    const choseNoLanguage = () =>{
        dispatch(resetLanguage());
        dispatch(closeFiltersModal());
    };
    return (
        <React.Fragment>
            <DialogTitle>Langue</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Choisissez la langues pour laquelle rechercher les articles
                </DialogContentText>
                <RadioGroup aria-label="language" name="language" value={languageFilterValue} onChange={handleLanguage}>
                    {
                        languageList.map((language, i)=> (
                            <FormControlLabel key={i} value={language.id} control={<Radio />} label={language.label} />
                        ))
                    }
                </RadioGroup>
            </DialogContent>
            <DialogActions className="actions-container">
                <Button onClick={choseNoLanguage} className="cta">
                    Pas de langue
                </Button>
            </DialogActions>
        </React.Fragment>
    );
}