import React from "react";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import {availableCountries} from './../../../redux/actions/constants.js';
import {useSelector, useDispatch} from 'react-redux';
import {closeFiltersModal, setCountry, resetCountry} from './../../../redux/actions/index.js';

export default function CountryModal() {
    const dispatch = useDispatch();
    const countryFilterValue = useSelector(state => state.country);
    const countriesList = availableCountries;
    const handleCountry = (e) =>{
        dispatch(setCountry(e.target.value));
        dispatch(closeFiltersModal());
    };
    const choseNoCountry = () =>{
        dispatch(resetCountry());
        dispatch(closeFiltersModal());
    };
    return (
        <React.Fragment>
            <DialogTitle>Pays</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Choisissez les pays dans lesquels rechercher les articles
                </DialogContentText>
                <RadioGroup aria-label="country" name="country" value={countryFilterValue} onChange={handleCountry}>
                    {countriesList.map ((country, i)=> (
                        <FormControlLabel key={i} value={country.id} control={<Radio />} label={country.label} />
                    ))}
                </RadioGroup>
            </DialogContent>
            <DialogActions className="actions-container">
                <Button onClick={choseNoCountry} className="cta">
                    Pas de pays
                </Button>
            </DialogActions>
        </React.Fragment>
    );
}