import React from "react";
import SourcesFilters from './SourcesFilters.js';
import FreshNewsFilters from './FreshNewsFilters.js';
import AllNewsFilters from './AllNewsFilters.js';
import {useSelector} from 'react-redux';

export default function ContentByAppContext() {
    const appContext = useSelector(state => state.appContext.label);
    return (
        appContext &&
            <React.Fragment>
                {appContext==="Articles récents" && <FreshNewsFilters/>}
                {appContext==="Tous les articles" && <AllNewsFilters/> }
                {appContext==="Sources" && <SourcesFilters/>}
            </React.Fragment>
    );
}