import React from "react";

export default function ModalError(props) {
    const errorCode = props.errorCode;
    return (
        <React.Fragment>
            Une erreur est survenue lors de la récupération de la liste des sources. Code d'erreur : {errorCode}
        </React.Fragment>
    );
}