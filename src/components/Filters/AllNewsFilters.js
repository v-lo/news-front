import React, {useState} from "react";
import FiltersChips from "./FiltersChips.js";
import FiltersModal from "./FiltersModal.js";
import SpeedDial from "@material-ui/lab/SpeedDial";
import SpeedDialIcon from "@material-ui/lab/SpeedDialIcon";
import SpeedDialAction from "@material-ui/lab/SpeedDialAction";
import KeywordIcon from "@material-ui/icons/FormatQuote";
import TranslateIcon from "@material-ui/icons/Translate";
import SupervisorAccountIcon from "@material-ui/icons/SupervisorAccount";
import HistoryIcon from "@material-ui/icons/History";
import UpdateIcon from "@material-ui/icons/Update";
import CloseIcon from "@material-ui/icons/Close";
import {useSelector,useDispatch} from 'react-redux';
import {openFiltersModal, setFiltersModalSubject} from './../../redux/actions/index.js';


export default function AllNewsFilters() {
    const dispatch = useDispatch();
    const [actionsOpen, showActionsButton] = useState(false);
    const keywordSelected = useSelector(state => state.keyword).length>0;
    const languageSelected = useSelector(state => state.language).length>0;
    const sourceSelected = useSelector(state => state.sourcesSelectedList).length>0;
    const dateFromSelected = (useSelector(state => state.dateFrom.active));
    const dateToSelected = (useSelector(state => state.dateTo.active));
    const addFilterButtonAvailable = !( keywordSelected && sourceSelected && languageSelected && dateFromSelected && dateToSelected);
    const fabActions =[
        { icon: <KeywordIcon />, name: "Mot-clés", able: !keywordSelected},
        { icon: <TranslateIcon />, name: "Langue", able: !languageSelected},
        { icon: <SupervisorAccountIcon />, name: "Sources", able: !sourceSelected},
        { icon: <HistoryIcon />, name: "Date minimale", able: !dateFromSelected},
        { icon: <UpdateIcon />, name: "Date maximale", able: !dateToSelected},
    ];
    const openModal = (filterSubject) =>{
        dispatch(setFiltersModalSubject(filterSubject));
        dispatch(openFiltersModal());
        showActionsButton(false);
    }
    return (
        <div className="filters-container">
            <FiltersChips/>
            {addFilterButtonAvailable &&
                <SpeedDial
                    className="filters-show-button-container"
                    ariaLabel="Filtres de recherche"
                    icon={<SpeedDialIcon openIcon={<CloseIcon />} />}
                    onClick={() => showActionsButton(!actionsOpen)}
                    open={true}//actionsOpen
                    direction="right"
                >
                    {fabActions.map(action => (
                        action.able &&
                            <SpeedDialAction
                                key={action.name}
                                icon={action.icon}
                                tooltipTitle={action.name}
                                onClick={() => openModal(action.name)}
                                tooltipPlacement="bottom-start"
                            />
                    ))}
                </SpeedDial>
            }
            <FiltersModal/>
        </div>
    );
}