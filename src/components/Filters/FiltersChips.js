import React from "react";
import {useSelector} from 'react-redux';
import KeywordChip from './Chips/KeywordChip.js';
import CountryChip from './Chips/CountryChip.js';
import ArticlesCategoryChip from './Chips/ArticlesCategoryChip.js';
import LanguageChip from './Chips/LanguageChip.js';
import DateFromChip from './Chips/DateFromChip.js';
import DateToChip from './Chips/DateToChip.js';
import SourcesChip from './Chips/SourcesChip.js';

export default function FiltersChips() {
    const countryFilterActive = useSelector(state => state.country).length>0;
    const DateToFilterActive = useSelector(state => state.dateTo.active);
    const DateFromFilterActive = useSelector(state => state.dateFrom.active);
    const languageFilterActive = useSelector(state => state.language).length>0;
    const sourceFilterActive = useSelector(state => state.sourcesSelectedList).length>0;
    const keywordFilterActive = useSelector(state => state.keyword).length>0;
    const articlesCategoryFilterActive = useSelector(state => state.articlesCategory).label.length>0;
    return (
        <React.Fragment>
            {keywordFilterActive && <KeywordChip/>}
            {countryFilterActive && <CountryChip/>}
            {articlesCategoryFilterActive && <ArticlesCategoryChip/>}
            {languageFilterActive && <LanguageChip/>}
            {DateFromFilterActive && <DateFromChip/>}
            {DateToFilterActive && <DateToChip/>}
            {sourceFilterActive && <SourcesChip/>}
        </React.Fragment>
    );
}