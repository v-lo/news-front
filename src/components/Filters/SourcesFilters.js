import React, {useState} from "react";
import FiltersChips from "./FiltersChips.js";
import FiltersModal from "./FiltersModal.js";
import SpeedDial from "@material-ui/lab/SpeedDial";
import SpeedDialIcon from "@material-ui/lab/SpeedDialIcon";
import SpeedDialAction from "@material-ui/lab/SpeedDialAction";
import LanguageIcon from "@material-ui/icons/Language";
import TranslateIcon from "@material-ui/icons/Translate";
import StyleIcon from "@material-ui/icons/Style";
import CloseIcon from "@material-ui/icons/Close";
import {useSelector,useDispatch} from 'react-redux';
import {openFiltersModal, setFiltersModalSubject} from './../../redux/actions/index.js';


export default function SourcesFilters() {
    const dispatch = useDispatch();
    const [actionsOpen, showActionsButton] = useState(false);
    const countrySelected = useSelector(state => state.country).length>0;
    const languageSelected = useSelector(state => state.language).length>0
    const articlesCategorySelected = useSelector(state => state.articlesCategory.label).length>0;
     const fabActions =[
        { icon: <StyleIcon />, name: "Catégorie", able: !articlesCategorySelected},
        { icon: <LanguageIcon />, name: "Pays", able: !countrySelected},
        { icon: <TranslateIcon />, name: "Langue", able: !languageSelected}
    ];
    const openModal = (filterSubject) =>{
        dispatch(setFiltersModalSubject(filterSubject));
        dispatch(openFiltersModal());
        showActionsButton(false);
    }
    return (
        <div className="filters-container">
            
            <FiltersChips/>
            <SpeedDial
                className="filters-show-button-container"
                ariaLabel="Filtres de recherche"
                icon={<SpeedDialIcon openIcon={<CloseIcon />} />}
                onClick={() => showActionsButton(!actionsOpen)}
                open={true}//actionsOpen
                direction="right"
            >
                {fabActions.map(action => (
                    action.able &&
                        <SpeedDialAction
                            key={action.name}
                            icon={action.icon}
                            tooltipTitle={action.name}
                            onClick={() => openModal(action.name)}
                            tooltipPlacement="bottom-start"
                        />
                ))}
            </SpeedDial>
            <FiltersModal/>
        </div>
    );
}