import React from "react";
import FetchFreshNews from './Fetch/FetchFreshNews.js';
import FetchAllNews from './Fetch/FetchAllNews.js';
import FetchSources from './Fetch/FetchSources.js';
import ResultsCount from './Utils/ResultsCount.js';
import Filters from './Filters/Filters.js';
import TopPagination from './Pagination/TopPagination.js';
import BottomPagination from './Pagination/BottomPagination.js';
import AppContextTitle from './Utils/AppContextTitle.js';
import {useSelector} from 'react-redux';

export default function ContentByAppContext() {
    const appContext = useSelector(state => state.appContext.label);
    return (
        appContext &&
            <React.Fragment>
                <AppContextTitle/>
                {appContext==="Articles récents" && 
                    <React.Fragment>
                        <Filters/>
                        <ResultsCount/>
                        <TopPagination/>
                        <FetchFreshNews/> 
                        <BottomPagination/>
                    </React.Fragment>
                }
                {appContext==="Tous les articles" && 
                    <React.Fragment>
                        <Filters/>
                        <ResultsCount/>
                        <TopPagination/>
                        <FetchAllNews/>
                        <BottomPagination/>
                    </React.Fragment> 
                }
                {appContext==="Sources" && 
                    <React.Fragment>
                        <Filters/>
                        <FetchSources/>
                    </React.Fragment>
                }
            </React.Fragment>
    );
}