import React, { Component } from 'react';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import SecurityIcon from '@material-ui/icons/Security';
import MailIcon from '@material-ui/icons/Mail';
import CircularProgress from '@material-ui/core/CircularProgress';
import axios from 'axios';
import sha256 from 'js-sha256';

class RegisterForm extends Component {
    
    constructor(props) {
      super(props);
      this.goLogin = this.props.goLogin.bind(this);
      this.state = {
        email : '',
        password : '',
        name : '',
        status: 'writing',
        errorMessage: ""
      };      
    }
    handlePseudoRegister = (e) => {
        this.setState({ name : e.target.value });
    };
    handleEmailRegister = (e) => {
        this.setState({ email : e.target.value });
    };
    handlePasswordRegister = (e) => {
        this.setState({ password : e.target.value });
    };

    /* REGISTER */
    submitRegister = (e) => {
        e.preventDefault();
        if(this.state.email && this.state.name && this.state.password.length>6){
          this.setState({status: "loading", errorMessage: ""});
          const dataEnvoi = {
            email : this.state.email,
            name : this.state.name,
            password : sha256(this.state.password)
          }
          return axios.post('http://localhost:8080/api/auth/register', dataEnvoi).then((response) =>{
            this.setState({status: "success"});
          }, (error) => {
            if(error.response.data){
              this.setState({status: "error", errorMessage: error.response.data});
            } else{
                this.setState({status: "error", errorMessage: "Une erreur est survenue. Le projet back est il lancé sur votre machine ? :)"});
            }
          })
        } else {
          this.setState({status: "error", errorMessage: "Le formulaire n'est pas correctement complété."});
        }
    };
  render() {
    const {status, errorMessage} = this.state;
      return (
        <div className="auth-box-container">
          {(status === "writing" || status === "error") &&
            <React.Fragment>
              <form onSubmit={this.submitRegister}>
                <TextField
                    autoFocus
                    id="register-pseudo"
                    label="Pseudo"
                    type="text"
                    fullWidth
                    onChange={e => this.handlePseudoRegister(e)}
                    required
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <AccountCircleIcon />
                        </InputAdornment>
                      ),
                    }}
                />
                <TextField
                    id="register-email"
                    label="Email"
                    type="email"
                    fullWidth
                    onChange={e => this.handleEmailRegister(e)}
                    required
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <MailIcon />
                        </InputAdornment>
                      ),
                    }}
                />
                <TextField
                    id="register-password"
                    label="Password"
                    type="password"
                    fullWidth
                    onChange={e => this.handlePasswordRegister(e)}
                    required
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <SecurityIcon />
                        </InputAdornment>
                      ),
                    }}
                />
                <input type="submit" value="S'enregistrer" className="authentication-button"/>
              </form>
              {status === "error" && 
                <div className="authentication-failed">
                  <p>Erreur : <span>{errorMessage}</span></p>
                </div>
              }
            </React.Fragment>
          }
          {status === "loading" && 
            <div className="auth-loader">
              <CircularProgress />
            </div>
          }
          {status === "success" && 
          <div className="authentication-succeed">
            <p>Votre compte a bien été créé. Connectez-vous désormais !</p>
            <button className="authentication-button" onClick={this.goLogin}>Me connecter</button>
          </div>
          }
        </div>
      );
  }
}

export default RegisterForm;