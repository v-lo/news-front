import React from 'react';
import {useSelector} from 'react-redux';
import AuthenticationTabs from './AuthenticationTabs';
import UserConnected from './UserConnected';

export default function HomeAuthentication() {
    const isUserConnected = useSelector(state => state.authentication.isLogged);
    return (
        isUserConnected ? <UserConnected/> : <AuthenticationTabs/>
    )
}