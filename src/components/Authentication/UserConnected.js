import React from 'react';
import {useSelector} from 'react-redux';

export default function UserConnected() {
    const userName = useSelector(state => state.authentication.userName);
    return (
        <div className="user-connected-container">
            <p>Bienvenue <span>{userName}</span>, vous êtes bien connecté</p>
        </div>
    );
}