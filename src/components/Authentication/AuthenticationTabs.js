import React from 'react';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import CreateIcon from '@material-ui/icons/Create';
import FaceIcon from '@material-ui/icons/Face';
import RegisterForm from './RegisterForm.js';
import LoginForm from './LoginForm.js';

export default function AuthenticationTabs() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const goToLoginForm = () =>{
    setValue(1);
  }

  return (
      <div className="authentication-tabs-container">
        <Tabs
          value={value}
          onChange={handleChange}
          variant="fullWidth"
          className="tabs-headers"
        >
          <Tab icon={<CreateIcon />} label="Créer un compte" />
          <Tab icon={<FaceIcon />} label="Se connecter" />
        </Tabs>
        {value === 0 && <RegisterForm goLogin={goToLoginForm}/>}
        {value === 1 && <LoginForm/>}
      </div>
  );
}