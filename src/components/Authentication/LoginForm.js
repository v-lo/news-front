import React, { Component } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import SecurityIcon from '@material-ui/icons/Security';
import MailIcon from '@material-ui/icons/Mail';
import axios from 'axios';
import sha256 from 'js-sha256';
import {setLoggedUser} from './../../redux/actions/index.js';
import {connect} from 'react-redux';

class LoginForm extends Component {
    state = {
        email : '',
        password : '',
        status: 'writing',
        errorMessage: ""
    };
    handlePasswordLogin = (e) => {
        this.setState({ password : e.target.value });
    };
    handleEmailLogin = (e) => {
        this.setState({ email : e.target.value });
    };
    submitAuthentification = (e) => {
        e.preventDefault();
        if(this.state.email && this.state.password){
            this.setState({status: "loading"});
            const dataEnvoi = {
                email : this.state.email,
                password : sha256(this.state.password)
            };
            return axios.post('http://localhost:8080/api/auth/login', dataEnvoi).then((response) =>{
                localStorage.setItem("myNewsUserInfos", JSON.stringify(response.data));
                this.setState({status: "success"});
                const loggedInfos = {
                    isLogged : true,
                    userName : response.data.name,
                    loggedUserId : response.data.jwt
                }
                this.props.setLoggedUser(loggedInfos);
            }, (error) => {
                if(error.response.data){
                    this.setState({status: "error", errorMessage: error.response.data});
                } else{
                    this.setState({status: "error", errorMessage: "Une erreur est survenue. Le projet back est il lancé sur votre machine ? :)"});
                }
            });
        } else {
            this.setState({status: "error", errorMessage: "Le formulaire n'est pas correctement complété."});
        }
    };

  render() {
      const {status, errorMessage} = this.state;
      return (
        <div className="auth-box-container">
            {(status === "writing" || status === "error") && 
                <React.Fragment>
                    <form onSubmit={this.submitAuthentification}>
                        <TextField
                            autoFocus
                            id="login-email"
                            label="Email"
                            type="email"
                            fullWidth
                            onChange={e => this.handleEmailLogin(e)}
                            required
                            InputProps={{
                                startAdornment: (
                                <InputAdornment position="start">
                                    <MailIcon />
                                </InputAdornment>
                                ),
                            }}
                        />
                        <TextField
                            id="login-password"
                            label="Password"
                            type="password"
                            fullWidth
                            onChange={e => this.handlePasswordLogin(e)}
                            required
                            InputProps={{
                                startAdornment: (
                                <InputAdornment position="start">
                                    <SecurityIcon />
                                </InputAdornment>
                                ),
                            }}
                        />
                        <input type="submit" value="Se connecter" className="authentication-button" />
                    </form>
                    {status === "error" && 
                        <div className="authentication-failed">
                        <p>Erreur : <span>{errorMessage}</span></p>
                        </div>
                    }
                </React.Fragment>
            }
            {status === "loading" &&
                <div className="auth-loader">
                    <CircularProgress />
                </div>
            }
            {status === "success" &&
                <div className="authentication-succeed">
                    <p>Vous êtes désormais connecté</p>
                </div>
            }
        </div>
      );
  }
}
export default connect(null, {setLoggedUser})(LoginForm);