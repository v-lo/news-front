import React, { Component } from 'react';
import {Switch, Route, Redirect}  from 'react-router-dom';
import {navigationTree, homePage } from './../redux/actions/constants.js';
import {connect} from 'react-redux';
import Main from"./Main.js";
import TopBar from"./TopBar.js";
import Home from './Home.js';
import LeftNav from"./LeftNav.js";

class Navigation extends Component {
    state={
        menuOpened: this.props.menuOpened,
        navigation : navigationTree
    }
    componentDidUpdate(nextProps){
        if(nextProps.menuOpened !== this.props.menuOpened){
           this.setState({menuOpened: this.props.menuOpened})
        }
    }
    render() {
        const visibilityMenu  = this.state.menuOpened ? 'menu-opened' : 'menu-closed';
        const {navigation} = this.state;
        return (
            <div className="all">
                <header className={`navigation ${visibilityMenu}`}>
                    <TopBar/>
                    <LeftNav/>
                </header>
                {/* ------ Contenu du main -------- */}
                <main className={`${visibilityMenu}`}>
                    <Switch>
                        {navigation.map((navigationItem, i) => (
                            <Route key={i} path={navigationItem.routerUrl}><Main appContext={navigationItem}/></Route>
                        ))}
                        <Route exact path="/"><Home appContext={homePage}/></Route>
                        <Redirect from="/" to="/"/>
                    </Switch>
                </main>
            </div>
        )}
}
const mapStateToProps = state => {
    return {
        menuOpened: state.menuOpened
    }
};
export default connect(mapStateToProps)(Navigation);