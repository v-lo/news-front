import React from 'react';
import {Link, withRouter}  from 'react-router-dom';
import {setAppContext} from './../redux/actions/index.js';
import {useDispatch} from 'react-redux';
import {navigationTree, homePage} from './../redux/actions/constants.js';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Icon from '@material-ui/core/Icon';

function LeftNav() {
    const dispatch = useDispatch();
    const navigation = navigationTree;
    const homePageAppContext = homePage;
    const navigate = (appContext) => {
        dispatch(setAppContext(appContext));
    }
    return (
        <div className="left-nav">
            <List component="nav" aria-label="links to articles categories" className="nav-list">
                <ListItem button component={Link} to={homePageAppContext.routerUrl} onClick={()=> navigate(homePageAppContext)} className="nav-list-item" selected={window.location.pathname ==='/'}>
                    <ListItemIcon className="subcat-icon">
                        <Icon>home</Icon>
                    </ListItemIcon>
                    <ListItemText primary="Accueil" />
                </ListItem>
                {navigation.map((navigationItem, i) => (
                    <ListItem button component={Link} onClick={()=> navigate(navigationItem)} to={navigationItem.routerUrl}  className="nav-list-item" selected={window.location.pathname === navigationItem.routerUrl} key={i}>
                        <ListItemIcon className="subcat-icon">
                            <Icon>{navigationItem.icone}</Icon>
                        </ListItemIcon>
                        <ListItemText primary={navigationItem.label} />
                    </ListItem>
                ))}
            </List>
        </div>
    )
}
export default withRouter(LeftNav);