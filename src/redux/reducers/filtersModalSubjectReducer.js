const filtersModalSubject = (state = "", action) => {
    switch(action.type){
        case 'SET_FILTERS_MODAL_SUBJECT':
            return action.payload;
        case 'RESET_FILTERS_MODAL_SUBJECT':
                return "";
        default :
            return state;
    }
}
export default filtersModalSubject;