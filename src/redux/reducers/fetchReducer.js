const fetchStatus = (state = "ok", action) => {
    switch(action.type){
        case 'SET_FETCH_STATUS':
            return action.payload;
        default :
            return state;
    }
}

export default fetchStatus;