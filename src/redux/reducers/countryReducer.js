const country = (state = "", action) => {
    switch(action.type){
        case 'SET_COUNTRY':
            return action.payload;
        case 'RESET_COUNTRY':
            return "";
        default :
            return state;
    }
}

export default country;