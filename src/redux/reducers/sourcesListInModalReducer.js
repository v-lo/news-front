const sourcesListInModal = (state = [], action) => {
    switch(action.type){
        case 'SET_SOURCES_LIST_IN_MODAL':
            return action.payload;
        case 'RESET_SOURCES_LIST_IN_MODAL':
            return [];
        default :
            return state;
    }
}

export default sourcesListInModal;