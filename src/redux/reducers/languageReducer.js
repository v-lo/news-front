const language = (state = "", action) => {
    switch(action.type){
        case 'SET_LANGUAGE':
            return action.payload;
        case 'RESET_LANGUAGE':
            return "";
        default :
            return state;
    }
}

export default language;