const dateFromSkeleton = {
    formated: "",
    raw: "",
    label: "",
    active: false
};
const dateFrom = (state = dateFromSkeleton, action) => {
    switch(action.type){
        case 'SET_DATE_FROM':
            return {
                active: true,
                formated: action.payload.toISOString(),
                raw: action.payload,
                stringified: action.payload.toLocaleDateString()
            };
        case 'RESET_DATE_FROM':
            return dateFromSkeleton;
        default :
            return state;
    }
}

export default dateFrom;