const errorCode = (state = "", action) => {
    switch(action.type){
        case 'SET_ERROR_CODE':
            return action.payload;
        default :
            return state;
    }
}
export default errorCode;