const dateToSkeleton = {
    active: false,
    formated: "",
    raw: "",
    label: ""
};
const dateTo = (state = dateToSkeleton, action) => {
    switch(action.type){
        case 'SET_DATE_TO':
            return {
                active: true,
                formated: action.payload.toISOString(),
                raw: action.payload,
                stringified: action.payload.toLocaleDateString()
            };
        case 'RESET_DATE_TO':
            return dateToSkeleton;
        default :
            return state;
    }
}

export default dateTo;