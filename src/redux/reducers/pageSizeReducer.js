const pageSize = (state = 20, action) => {
    switch(action.type){
        case 'SET_PAGE_SIZE':
            return action.payload;
        case 'RESET_PAGE_SIZE':
            return 20;
        default :
            return state;
    }
}

export default pageSize;