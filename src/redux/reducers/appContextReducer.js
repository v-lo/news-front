const appContextSkeleton = {
    label : "",
    icone : "",
    fetchUrl : "",
    routerUrl : ""
}

const appContext = (state = appContextSkeleton, action) => {
    switch(action.type){
        case 'SET_APP_CONTEXT':
            return action.payload;
        default :
            return state;
    }
}

export default appContext;