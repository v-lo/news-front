const articlesResultsNumber = (state = 0, action) => {
    switch(action.type){
        case 'SET_ARTICLES_RESULTS_NUMBER':
            return action.payload;
        case 'RESET_ARTICLES_RESULTS_NUMBER':
            return 0;
        default :
            return state;
    }
}

export default articlesResultsNumber;