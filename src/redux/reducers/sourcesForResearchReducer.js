const sourcesSelectedList = (state = [], action) => {
    switch(action.type){
        case 'SET_SOURCES_FOR_RESEARCH_LIST':
            return action.payload;
        case 'RESET_SOURCES_FOR_RESEARCH_LIST':
            return [];
        default :
            return state;
    }
}

export default sourcesSelectedList;