const isFiltersModalOpen = (state = false, action) => {
    switch(action.type){
        case 'OPEN_FILTERS_MODAL':
            return true;
        case 'CLOSE_FILTERS_MODAL':
            return false;
        default :
            return state;
    }
}

export default isFiltersModalOpen;