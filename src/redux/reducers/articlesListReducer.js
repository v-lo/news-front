const articlesList = (state = [], action) => {
    switch(action.type){
        case 'SET_ARTICLES_LIST':
            return action.payload;
        case 'RESET_ARTICLES_LIST':
            return [];
        default :
            return state;
    }
}

export default articlesList;