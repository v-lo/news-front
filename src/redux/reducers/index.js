import menuOpened from './menuOpenedReducer.js';
import appContext from './appContextReducer.js';
import articlesCategory from './articlesCategoryReducer.js';
import authentication from './authenticationReducer.js';
import articlesResultsNumber from './articlesResultsNumberReducer.js';
import articlesList from './articlesListReducer.js';
import sourcesList from './sourcesListReducer.js';
import sourcesSelectedList from './sourcesForResearchReducer.js';
import sourcesListInModal from './sourcesListInModalReducer.js';
import fetchStatus from './fetchReducer.js';
import errorCode from './errorReducer.js';
import isFiltersModalOpen from './filtersModalReducer.js';
import filtersModalSubject from './filtersModalSubjectReducer.js';
import pageSize from './pageSizeReducer.js';
import pageNumber from './pageNumberReducer.js';
import country from './countryReducer.js';
import keyword from './keywordReducer.js';
import language from './languageReducer.js';
import dateFrom from './dateFromReducer.js';
import dateTo from './dateToReducer.js';
import {combineReducers} from 'redux';

export default combineReducers({
    menuOpened, appContext,authentication,//Global
    isFiltersModalOpen, filtersModalSubject, articlesCategory, keyword, country, language, dateFrom, dateTo, sourcesListInModal, sourcesSelectedList,//Filters
    articlesList, sourcesList, pageSize, pageNumber,articlesResultsNumber,//Results/params
    fetchStatus, errorCode//API dialog
});