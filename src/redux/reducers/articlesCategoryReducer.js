const articlesCategorySkeleton = {
    label : "",
    icone : "",
    fetchUrl : "",
    path : ""
};

const articlesCategory = (state = articlesCategorySkeleton, action) => {
    switch(action.type){
        case 'SET_ARTICLES_CATEGORY':
            return action.payload;
        case 'RESET_ARTICLES_CATEGORY':
            return articlesCategorySkeleton;
        default :
            return state;
    }
}

export default articlesCategory;