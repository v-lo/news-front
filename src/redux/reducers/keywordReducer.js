const keyword = (state = "", action) => {
    switch(action.type){
        case 'SET_KEYWORD':
            return action.payload;
        case 'RESET_KEYWORD':
            return "";
        default :
            return state;
    }
}

export default keyword;