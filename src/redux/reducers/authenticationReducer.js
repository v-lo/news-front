const authSkeleton = {
    isLogged : false,
    userName : "",
    loggedUserId : ""
}

const authentication = (state = authSkeleton, action) => {
    switch(action.type){
        case 'SET_LOGGED_USER':
            return action.payload;
        case 'SET_LOGOUT_USER':
            return authSkeleton;
        default :
            return authSkeleton;
    }
}

export default authentication;