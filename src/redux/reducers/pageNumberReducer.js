const pageNumber = (state = 1, action) => {
    switch(action.type){
        case 'SET_PAGE_NUMBER':
            return action.payload;
        case 'GO_NEXT_PAGE':
            return state + 1;
        case 'GO_PREVIOUS_PAGE':
            return state - 1;
        case 'RESET_PAGE_NUMBER':
            return 1;
        default :
            return state;
    }
}

export default pageNumber;