export const  urlBase = "https://newsapi.org/v2/";
export const apiKey = "9d9a6eed3cb541ee80b0adf628c0f744";
export const homePage = {
    label : "Accueil",
    fetchUrl : "",
    icone: "home",
    routerUrl: "/accueil"
};
export const navigationTree = [
    {
        label : "Articles récents",
        fetchUrl : "top-headlines?",
        icone: "whatshot",
        routerUrl: "/articles-recents"
    },
    {
        label : "Tous les articles",
        fetchUrl : "everything?",
        icone: "list",
        routerUrl: "/tous-les-articles"
    },
    {
        label : "Sources",
        fetchUrl : "sources?",
        icone: "supervisor_account",
        routerUrl: "/sources"
    }
];
export const categoriesList = [
    {
        label : "Général",
        icone : "description",
        fetchUrl : "general"
    },
    {
        label : "Économie",
        icone : "account_balance",
        fetchUrl : "business"
    },
    {
        label : "Santé",
        icone : "filter_vintage",
        fetchUrl : "health"
    },
    {
        label : "Sciences",
        icone : "colorize",
        fetchUrl : "science"
    },
    {
        label : "Sport",
        icone : "sports_soccer",
        fetchUrl : "sports"
    },
    {
        label : "Technologie",
        icone : "memory",
        fetchUrl : "technology"
    },
    {
        label : "Divertissement",
        icone : "sentiment_very_satisfied",
        fetchUrl : "entertainment"
    }
];
export const availableLanguages = [
    {
        id: "ar",
        label : "Arabe"
    },{
        id: "de",
        label : "Allemand"
    },{
        id: "en",
        label : "Anglais"
    },{
        id: "es",
        label : "Espagnol"
    },{
        id: "fr",
        label : "Français"
    },{
        id: "he",
        label : "Hébreu"
    },{
        id: "it",
        label : "Italien"
    },{
        id: "nl",
        label : "Néerlandais"
    },{
        id: "no",
        label : "Norvégien"
    },{
        id: "pt",
        label : "Portugais"
    },{
        id: "ru",
        label : "Russe"
    },{
        id: "se",
        label : "Same du Nord"
    },{
        id: "zh",
        label : "Chinois"
    }
];
export const availableCountries = [
    {
        id: "ae",
        label : "Émirats arabes unis"
    },{
        id: "ar",
        label : "Argentine"
    },{
        id: "at",
        label : "Autriche"
    },{
        id: "au",
        label : "Australie"
    },{
        id: "be",
        label : "Belgique"
    },{
        id: "bg",
        label : "Bulgarie"
    },{
        id: "br",
        label : "Brésil"
    },{
        id: "ca",
        label : "Canada"
    },{
        id: "ch",
        label : "Suisse"
    },{
        id: "cn",
        label : "Chine"
    },{
        id: "co",
        label : "Colombie"
    },{
        id: "cu",
        label : "Cuba"
    },{
        id: "cz",
        label : "Tchéquie"
    },{
        id: "de",
        label : "Allemagne"
    },{
        id: "eg",
        label : "Égypte"
    },{
        id: "fr",
        label : "France"
    },{
        id: "gb",
        label : "Royaume-Uni"
    },{
        id: "gr",
        label : "Grèce"
    },{
        id: "hk",
        label : "Hong Kong"
    },{
        id: "hu",
        label : "Hongrie"
    },{
        id: "id",
        label : "Indonésie"
    },{
        id: "ie",
        label : "Irlande"
    }, {
        id: "il",
        label : "Israël"
    },{
        id: "in",
        label : "Inde"
    },{
        id: "it",
        label : "Italie"
    },{
        id: "jp",
        label : "Japon"
    },{
        id: "kr",
        label : "Corée du Sud"
    },{
        id: "lt",
        label : "Lituanie"
    },{
        id: "lv",
        label : "Lettonie"
    },{
        id: "ma",
        label : "Maroc"
    }, {
        id: "mx",
        label : "Mexique"
    },{
        id: "my",
        label : "Malaysie"
    },{
        id: "ng",
        label : "Nigeria"
    },{
        id: "nl",
        label : "Pays-Bas"
    }, {
        id: "no",
        label : "Norvège"
    },{
        id: "nz",
        label : "Nouvele-Zélande"
    },{
        id: "ph",
        label : "Philippines"
    },{
        id: "pl",
        label : "Pologne"
    },{
        id: "pt",
        label : "Portugal"
    },{
        id: "ro",
        label : "Roumanie"
    },{
        id: "rs",
        label : "Serbie"
    },{
        id: "ru",
        label : "Russie"
    },{
        id: "sa",
        label : "Arabie Saoudite"
    },{
        id: "se",
        label : "Suède"
    },{
        id: "sg",
        label : "Singapour"
    },{
        id: "si",
        label : "Slovénie"
    },{
        id: "sk",
        label : "Slovaquie"
    },{
        id: "th",
        label : "Thaïlande"
    },{
        id: "tr",
        label : "Turquie"
    },{
        id: "tw",
        label : "Taïwan"
    },{
        id: "ua",
        label : "Ukraine"
    },{
        id: "us",
        label : "États-Unis"
    },{
        id: "ve",
        label : "Venezuela"
    },{
        id: "za",
        label : "Afrique du Sud"
    }
];