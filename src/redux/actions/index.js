export const toggleMenu = () =>{
    return {type: 'TOGGLE_MENU'};
};
export const setAppContext= (value) =>{
    return {type: 'SET_APP_CONTEXT',payload: value};
};
export const setLoggedUser= (value) =>{
    return {type: 'SET_LOGGED_USER',payload: value};
};
export const setLogoutUser = () =>{
    return {type: 'SET_LOGOUT_USER'};
};
//Fetch
export const setFetchStatus = (value) =>{
    return {type: 'SET_FETCH_STATUS',payload: value};
};
export const setErrorCode = (value) =>{
    return {type: 'SET_ERROR_CODE',payload: value};
};
export const setArticlesList = (value) =>{
    return {type: 'SET_ARTICLES_LIST',payload: value};
};
export const resetArticlesList = () =>{
    return {type: 'RESET_ARTICLES_LIST'};
};
export const setSourcesList = (value) =>{
    return {type: 'SET_SOURCES_LIST',payload: value};
};
export const resetSourcesList = () =>{
    return {type: 'RESET_SOURCES_LIST'};
};
export const setArticlesResultsNumber = (value) =>{
    return {type: 'SET_ARTICLES_RESULTS_NUMBER',payload: value};
};
export const resetArticlesResultsNumber = () =>{
    return {type: 'RESET_ARTICLES_RESULTS_NUMBER'};
};
//Filters
export const openFiltersModal = () =>{
    return {type: 'OPEN_FILTERS_MODAL'};
};
export const closeFiltersModal = () =>{
    return {type: 'CLOSE_FILTERS_MODAL'};
};
export const setFiltersModalSubject = (value) =>{
    return {type: 'SET_FILTERS_MODAL_SUBJECT',payload: value};
};
export const resetFiltersModalSubject = () =>{
    return {type: 'RESET_FILTERS_MODAL_SUBJECT'};
};
export const setArticlesCategory = (value) =>{
    return {type: 'SET_ARTICLES_CATEGORY',payload: value};
};
export const resetArticlesCategory = () =>{
    return {type: 'RESET_ARTICLES_CATEGORY'};
};
export const setCountry = (value) =>{
    return {type: 'SET_COUNTRY',payload: value};
};
export const resetCountry = () =>{
    return {type: 'RESET_COUNTRY'};
};
export const setKeyword = (value) =>{
    return {type: 'SET_KEYWORD',payload: value};
};
export const resetKeyword = () =>{
    return {type: 'RESET_KEYWORD'};
};
export const setLanguage = (value) =>{
    return {type: 'SET_LANGUAGE',payload: value};
};
export const resetLanguage = () =>{
    return {type: 'RESET_LANGUAGE'};
};
export const setDateFrom = (value) =>{
    return {type: 'SET_DATE_FROM',payload: value};
};
export const resetDateFrom = () =>{
    return {type: 'RESET_DATE_FROM'};
};
export const setDateTo = (value) =>{
    return {type: 'SET_DATE_TO',payload: value};
};
export const resetDateTo = () =>{
    return {type: 'RESET_DATE_TO'};
};
export const setSourcesForResearchList = (value) =>{
    return {type: 'SET_SOURCES_FOR_RESEARCH_LIST',payload: value};
};
export const resetSourcesForResearchList = () =>{
    return {type: 'RESET_SOURCES_FOR_RESEARCH_LIST'};
};
export const setSourcesListInModal = (value) =>{
    return {type: 'SET_SOURCES_LIST_IN_MODAL',payload: value};
};
export const resetSourcesListInModal = () =>{
    return {type: 'RESET_SOURCES_LIST_IN_MODAL'};
};

//Pagination
export const setPageSize = (value) =>{
    return {type: 'SET_PAGE_SIZE', payload: value};
};
export const resetPageSize = () =>{
    return {type: 'RESET_PAGE_SIZE'};
};
export const setPageNumber = (value) =>{
    return {type: 'SET_PAGE_NUMBER',payload: value};
};
export const resetPageNumber = () =>{
    return {type: 'RESET_PAGE_NUMBER'};
};
export const goNextPage = () =>{
    return {type: 'GO_NEXT_PAGE'};
};
export const goPreviousPage = () =>{
    return {type: 'GO_PREVIOUS_PAGE'};
};